#include <unitest.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <errno.h>

#include <hash_table.h>
#include <utils/hash_table.h>

void
insert_strings() {
    char* k;
    void* v;
    void* iter;
    h_table_t ht;
    char *str[] = {"Kick", "vev", "btbtrt", "ntsrb", "btsnt", NULL};
    char** ptr = str;

    h_table_init(&ht);
    while(*ptr){
        char* value = malloc(sizeof(char) * 20);

        strcpy(value, *ptr);
        h_insert(&ht, *ptr, value);
        T_ASSERT_STRING((char*)h_lookup(&ht, *ptr), *ptr);

        ptr++;
    }
    display(&ht);
    T_ASSERT_NUM(ht.size, 5);
    T_ASSERT_NUM(ht.capacity, 256);
    T_ASSERT_NUM(ht.depth, 4);

    iter = h_iter(&ht);
    while( !h_next(iter, &k, &v) ) {
        printf("Freeing %s\n", (char*)v);
        free(h_delete(&ht, k));
    }
    free(iter);

    h_table_free(&ht);
}

void
insert_one() {
    int a = 19;
    int b = 49;
    char str[] = {'a', 0};
    h_table_t* ht = malloc(sizeof(h_table_t));

    h_table_init(ht);
    T_ASSERT_NUM(h_insert(ht, str, &a), 0);
    T_ASSERT_NUM(*((int*)h_lookup(ht, str)), a);

    T_ASSERT_NUM(h_insert(ht, "k", NULL), 0);
    T_ASSERT_NUM(*((int*)h_lookup(ht, str)), a);

    h_update(ht, str, &b);
    T_ASSERT_NUM(*((int*)h_lookup(ht, str)), b);

    h_table_free(ht);
    free(ht);
}

void
test_iterator() {
    void* iter;
    char* k = NULL;
    void* v = NULL;
    char test1[] = "Test string";
    char test2[] = "Another test string";
    h_table_t ht;

    h_table_init(&ht);
    h_insert(&ht, "a", test1);
    h_insert(&ht, "b", test2);

    iter = h_iter(&ht);
    T_ASSERT_NUM(h_next(iter, &k, &v), 0);
    T_ASSERT_STRING(k, "b");
    T_ASSERT_STRING((char*)v, test2);
    T_ASSERT_NUM(h_next(iter, &k, &v), 0);
    T_ASSERT_STRING(k, "a");
    T_ASSERT_STRING((char*)v, test1);
    T_ASSERT_NUM(h_next(iter, &k, &v), 1);

    h_table_free(&ht);
    free(iter);
}

void
test_delete() {
    int num = 19;
    int* ptr = &num;
    char str[] = "This is a string.";
    h_table_t ht;
    char key[5] = "";

    h_table_init(&ht);

    h_int_to_str(4, key);
    T_ASSERT_NUM(h_insert(&ht, key, ptr), 0);
    h_int_to_str(43, key);
    T_ASSERT_NUM(h_insert(&ht, key, str), 0);
    T_ASSERT_NUM(ht.size, 2);
    T_ASSERT_NUM(ht.depth, 1);

    h_int_to_str(4, key);
    T_ASSERT_NUM(h_lookup(&ht, key), ptr);
    h_int_to_str(43, key);
    T_ASSERT_NUM(h_lookup(&ht, key), str);

    T_ASSERT_NUM(ht.heap[find_bachelor(&ht, 1)]->value, str);

    h_int_to_str(4, key);
    T_ASSERT_NUM(h_delete(&ht, key), ptr);
    h_int_to_str(43, key);
    T_ASSERT_NUM(h_delete(&ht, key), str);
    T_ASSERT_NUM(ht.size, 0);
    T_ASSERT_NUM(ht.depth, 0);
    h_table_free(&ht);
}

void
test_delete_bad_keys() {
    h_table_t ht;

    h_table_init(&ht);
    T_ASSERT(!h_delete(&ht, ""));
    T_ASSERT_NUM(errno, ENOENT);
    T_ASSERT(!h_delete(&ht, "unknown"));
    T_ASSERT_NUM(errno, ENOENT);
    T_ASSERT_NUM(ht.size, 0);
    h_table_free(&ht);
}

void
test_set_empty_key() {
    h_table_t ht;

    h_table_init(&ht);
    T_ASSERT_NUM(h_insert(&ht, "", NULL), 1);
    T_ASSERT_NUM(ht.size, 0);
    h_table_free(&ht);
}

void
test_reinsert() {
    char test1[] = "Test string";
    char test2[] = "Another test string";
    h_table_t ht;

    h_table_init(&ht);

    T_ASSERT_NUM(h_insert(&ht, "kick", test1), 0);
    T_ASSERT_NUM(h_insert(&ht, "kick", test2), 0);
    h_table_free(&ht);
}

void
test_lookup_no_exist() {
    h_table_t ht;

    h_table_init(&ht);
    T_ASSERT(!h_lookup(&ht, "Does not exists"))
    T_ASSERT_NUM(errno, ENOENT);
    h_table_free(&ht);
}

void
test_stress() {
    uint64_t i;
    h_table_t ht;

    h_table_init(&ht);

    i = 0; while ( i++ <= 100 ) {
        char key[1024] = {0};
        sprintf(key, "%ld", i);
        T_ASSERT_NUM(h_insert(&ht, key, (void*)i), 0);
    }
    h_table_free(&ht);
}

int
main(void){
    T_SUITE(Hash Table Tests,
        TEST(Insert one string, insert_one());

        TEST(Insert String into hash, insert_strings());

        TEST(Lookup no exist, test_lookup_no_exist());

        TEST(Re-insert, test_reinsert());

        TEST(Set empty key, test_set_empty_key());

        TEST(Delete bad keys, test_delete_bad_keys());

        TEST(Delete keys, test_delete());

        TEST(Iterator, test_iterator());

        /* TEST(Test Stress, test_stress()); */
    );

    T_CONCLUDE();
    return 0;
}
