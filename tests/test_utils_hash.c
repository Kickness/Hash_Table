#include <unitest.h>
#include <stdio.h>
#include <malloc.h>

#include <hash_table.h>
#include <utils/hash_table.h>

void
get_unknown_key(h_table_t* ht){
    int64_t h;

    h = hash("unknown", 2);
    T_ASSERT_NUM(h, 1);
    h = hash("", 2);
    T_ASSERT_NUM(h, 0);
}

void
get_unknown_entry(h_table_t* ht){
    uint64_t i;

    i = get_entry(ht, "unknown");
    T_ASSERT_NUM(i, 0);
    i = get_entry(ht, "");
    T_ASSERT_NUM(i, 0);
}

int main(void){
    h_table_t ht;

    h_table_init(&ht);

    T_SUITE(Test Utils,
        TEST(Get unknown key, get_unknown_key(&ht));

        TEST(Get unknown entry, get_unknown_entry(&ht));
    );

    h_table_free(&ht);
    T_CONCLUDE();
    return 0;
}
