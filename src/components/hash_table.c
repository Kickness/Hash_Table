#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include <hash_table.h>
#include <utils/hash_table.h>

#define START_SIZE (1 << 8)
#define MAX(a, b) ((a > b) ? a : b)
#define MIN(a, b) ((a < b) ? a : b)

double log2(double);  /* math.h was not working */

int
h_table_init(h_table_t* ht){
    ht->capacity = START_SIZE;
    ht->size = 0;
    ht->depth = 0;
    ht->heap = calloc(ht->capacity, sizeof(h_node_t*));

    return 0;
}

static void
h_node_free(h_node_t* hn){
    free(hn->key);
}

void
h_table_free(h_table_t* ht){
    uint32_t i = 0;

    while(++i < ht->capacity) {
        if (!ht->heap[i]) continue;

        h_node_free(ht->heap[i]);
        free(ht->heap[i]);
    }
    free(ht->heap);
}

int
h_insert(h_table_t* ht, const char* k, void* v){
    uint64_t i, key_size = strlen(k);
    h_node_t** heap;

    if (!key_size) return 1;

    if ( (1 << (ht->depth + 1)) >= ht->capacity) {
        uint32_t old_cap = ht->capacity;

        ht->capacity *= 2;
        ht->heap = realloc(ht->heap, sizeof(h_node_t*) * ht->capacity);
        memset(ht->heap + old_cap, 0, sizeof(h_node_t*) * old_cap);
    }
    heap = ht->heap;

    i = empty_index(ht, k, key_size);

    heap[i] = malloc(sizeof(h_node_t));
    heap[i]->key = malloc(sizeof(char) * (key_size + 1));
    strcpy(heap[i]->key, k);
    heap[i]->value = v;
    ht->size++;
    ht->depth = MAX(ht->depth, (uint32_t) log2( (double)i ));
    return 0;
}

int
h_update(h_table_t* ht, const char* k, void* v){
    uint64_t i = get_entry(ht, k);
    if(!i){
        errno = ENOENT;
        return 1;
    }
    ht->heap[i]->value = v;
    return 0;
}

void*
h_lookup(h_table_t* ht, const char* k){
    uint64_t i = get_entry(ht, k);

    if (!i) {
        errno = ENOENT;
        return NULL;
    }
    return ht->heap[i]->value;
}

static void*
remove_entry(h_table_t* ht, uint64_t i) {
    void* value;
    uint64_t bachelor;

    if (!i) {
        errno = ENOENT;
        return NULL;
    }

    bachelor = find_bachelor(ht, i);

    value = ht->heap[i]->value;
    h_node_free(ht->heap[i]);
    free(ht->heap[i]);
    ht->heap[i] = NULL;

    if (bachelor) {
        ht->heap[i] = ht->heap[bachelor];
        ht->heap[bachelor] = NULL;
    }

    ht->size--;
    ht->depth = MIN(ht->depth, (uint32_t) log2((double)bachelor));
    return value;
}

void*
h_delete(h_table_t* ht, const char* k) {
    uint64_t i = get_entry(ht, k);

    return remove_entry(ht, i);
}

h_iter_t*
h_iter(h_table_t* ht){
    h_iter_t* iter = malloc(sizeof(h_iter_t));
    iter->table = ht;
    iter->i = ht->capacity;
    return iter;
}

int
h_next(h_iter_t* hi, char** k, void** v){
    h_table_t* ht = hi->table;

    while (--hi->i > 1 && !ht->heap[hi->i]);
    if (hi->i < 1) return 1;

    *k = ht->heap[hi->i]->key;
    *v = ht->heap[hi->i]->value;
    return 0;
}

void
h_int_to_str(int k, char* key){
    int i = 0;
    while(i < sizeof(k)){
        key[i] = ((char)k) + 'a';
        k = k >> 8 * sizeof(char) * ++i;
    }
}

