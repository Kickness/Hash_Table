#ifndef _HASH_TABLE_H
#define _HASH_TABLE_H

#include <stdint.h>

typedef struct h_node{
    char* key;
    void* value;
} h_node_t;

typedef struct {
    uint32_t capacity;
    uint32_t size;
    uint32_t depth;

    h_node_t** heap;
} h_table_t;

typedef struct{
    h_table_t* table;
    uint64_t i;
} h_iter_t;

int h_table_init(h_table_t*);
void h_table_free(h_table_t*);
h_iter_t* h_iter(h_table_t*);
int h_next(h_iter_t*, char**, void**);
int h_insert(h_table_t*, const char*, void*);
void* h_delete(h_table_t*, const char*);
void* h_lookup(h_table_t*, const char*);
int h_update(h_table_t*, const char*, void*);
void h_int_to_str(int k, char* key);

#endif /* END OF _HASH_TABLE_H */
