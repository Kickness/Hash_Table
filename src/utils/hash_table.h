#ifndef _UTILS_HASH_H
#define _UTILS_HASH_H

#include <stdint.h>
#include <hash_table.h>

uint64_t
mod(int64_t a, int64_t b);

uint64_t
hash(const char* k, uint64_t limit);

void
display(h_table_t* ht);

uint64_t
get_entry(h_table_t* ht, const char* k);

uint64_t
empty_index(h_table_t* ht, const char* k, uint32_t key_size);

uint64_t
find_bachelor(h_table_t* ht, uint64_t);

#endif
