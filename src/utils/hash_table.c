#include <math.h>
#include <string.h>
#include <stdio.h>

#include <hash_table.h>
#include <utils/hash_table.h>

#define RIGHT(x) (2 * x + 1)
#define LEFT(x) (2 * x)

uint64_t
mod(int64_t a, int64_t b){
    return a - b * floor((double)a / (double)b);
}

uint64_t
hash(const char* k, uint64_t limit){
    return mod(*k, limit);
}

static uint64_t
next_index(uint64_t index, const char* k, uint32_t key_size) {
    const char* letter = k + mod(index - 1, key_size);
    uint64_t h = hash(letter, 2);
    uint8_t divided = mod((*letter - 'a') + index, 2);

    /* printf("Key: %s, index: %ld, hash: %ld, divided: %d, letter: %d\n", k, index, h, divided, (*letter - 'a')); */
    return (2 * index) + (h ^ divided);
}

void
display(h_table_t* ht) {
    uint64_t i;

    printf("capacity: %d\n", ht->capacity);
    i = 0; while ( i < ht->capacity )
        printf("%p ", (void*) ht->heap[i++]);
    printf("\n");
}

uint64_t
empty_index(h_table_t* ht, const char* k, uint32_t key_size) {
    uint64_t i;
    h_node_t** heap = ht->heap;

    i = 1; while ( heap[i] ) i = next_index(i, k, key_size);
    return i;
}

uint64_t
get_entry(h_table_t* ht, const char* k){
    uint64_t i;
    uint64_t key_size = strlen(k);
    h_node_t** heap = ht->heap;

    i = 1; while ( heap[i] && strcmp(heap[i]->key, k) )
        i = next_index(i, k, key_size);
    return heap[i] ? i : 0;
}

uint64_t
find_bachelor(h_table_t* ht, uint64_t i){
    h_node_t** heap = ht->heap;

    if (!heap[i]) return 0;
    while ( heap[RIGHT(i)] || heap[LEFT(i)] ) {
        while ( heap[RIGHT(i)] ) i = RIGHT(i);
        while ( heap[LEFT(i)] ) i = LEFT(i);
    }
    return i;
}
